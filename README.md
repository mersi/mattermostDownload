# Mattermost downloader

These scripts download the mattermost conversation and convert them into html.
The first part was [originally written](https://gist.github.com/RobertKrajewski/5847ce49333062ea4be1a08f2913288c) by Robert Krajewski and adapted for linux by me.

## Installation
```bash
pip3 install --user mattermostdriver
```

## Instructions
Provided you use linux with firefox, and OAuth aothentication, you have to login mattermost with firefox.
Then you start the two scripts in sequence: ```mattermost-dl.py``` and then  ```mattermost2html.py```

### Downloading the data
  - run ```mattermost-dl.py```, then you input the address of your mattermost server, then answer `token` to use the OAuth, then answer `y`.
  - if you store the configuration, then be aware that when the token will have expired, you will not be able to connect any more. In that case remove the json file with the configuration with `rm config.json`
  - After it says `Successfully logged in as username`, be patient
  - after everything is downloaded a directory will be created with the name `results/current_date/channel_name/` . Take note of that, because you will need it

### Convert the chat data into html
  - go into the directory you just created. If you only downloaded a single file you can do `cd results/*/*`
  - run the conversion script ```../../../mattermost2html.py ```                                                                                                      




 
