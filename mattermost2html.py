#!/usr/bin/env python3

import os
import json

jsonFileName=os.path.basename(os.getcwd())+'.json'

# Load JSON data
with open(jsonFileName, 'r') as file:
    data = json.load(file)
    chat_data = data['posts']

# Start building HTML content
html_content = """
<!DOCTYPE html>
<html>
<head>
    <title>Chat History</title>
"""

# Extract unique participants from chat_data
participants = set(entry["username"] for entry in chat_data)

# List of colors
colors = [
    "#004586", "#FF420E", "#FFD320", "#579D1C", "#7E0021", "#83CAFF",
    "#314004", "#AECF00", "#4B1F6F", "#FF950E", "#C5000B", "#0084D1"
]

# Map participants to colors
color_mapping = {}
for idx, participant in enumerate(participants):
    color_mapping[participant] = colors[idx % len(colors)]

# Generate the <style> section
style_content = "<style>\n"
for participant, color in color_mapping.items():
    style_content += f".{participant} {{ color: {color}; font-weight: bold; }}\n"
style_content += "</style>"

html_content += style_content

html_content += """    </style>
</head>
<body>
    <h1>Chat History</h1>
    <ul>
"""

def format_message(message):
    parts = message.split("```")
    is_code_block = False
    formatted_parts = []

    for part in parts:
        if is_code_block:
            formatted_parts.append(f"<pre>{part}</pre>")
        else:
            formatted_parts.append(part)
        
        is_code_block = not is_code_block

    return ''.join(formatted_parts)


# Add chat messages to HTML content
for entry in chat_data:
    formatted_message = format_message(entry["message"])

    message_html = f'<li><span class="{entry["username"]}">{entry["username"]}</span>: {formatted_message} <small>({entry["created"]})</small>'

    # If there are files attached, process them
    if "files" in entry:
        for file_path in entry["files"]:
            if file_path.lower().endswith(('.png', '.jpg', '.jpeg')):
                # Embed the image
                message_html += f'<br><a href="{file_path}" target="_blank"><img src="{file_path}" width="200px" alt="Image attachment"></a>'
            else:
                # Provide an attachment link
                message_html += f'<br><a href="{file_path}" target="_blank">Attachment</a>'

    message_html += '</li>'
    html_content += message_html


html_content += """
    </ul>
</body>
</html>
"""

# Write HTML content to file
with open("chat.html", 'w') as output_file:
    output_file.write(html_content)

print("HTML content written to chat.html!")
